#!/usr/bin/python3

from functool import total_ordering

@total_ordering
class Card(object):
	face_cards = {'J': 11, 'Q': 12, 'K': 13, 'A': 14}

	def __init__(self, value, colour):
		self.face_value = value
		self.value = self.face_cards.get(value)
		self.colour = colour

	def __repr__(self):
		return self.face_value + self.colour

	def __eq__(self, other):
		return self.value == other.value

	def __lt__(self, other):
		return self.value < other.value

	def consecutive(self, other):
		return abs(self.value - other.value) == 1

class Hand(object):

	def __init__(self, cards):
		self.cards = cards

	@staticmethod
	def consecutive(cards):
		for i in range(len(cards)-1):
			if cards[i].consecutive(cards[i+1]):
				return False
		return True

	@staticmethod
	def equal_colour(cards):
		return cards and bool([c for c in cards if c.colour == c[0].colour])

	@staticmethod
	def equal_value(cards):
		return cards and bool([c for c in cards if c.value == c[0].value])

	def evaluate(self):
		remainder = self.cards

		while remainder:
			for combination in self.combinations:
				found, remainder = combination(remainder)

#	def royal_flush(self, remainder):
#		face_cards = [card for card in remainder if card.value > 10]
#		if len(face_cards) == 5 and self.consecutive(face_cards):
#			return True, []

	@classmethod
	def straight_flush(cls, cards):
		if cls.straight(cards) and cls.flush(cards):
			return cards, []
		else:
			return [], cards

	@classmethod
	def poker(cls, cards):
		if len(cards) >= 4 and equal_value(cards[:4]):
			return cards[:4], cards[4:]
		return [], cards

	@classmethod
	def full_house(cls, cards):


	def flush(cards):
		if 

	@classmethod
	def straight(cls, cards):
		return cls.consecutive(cards)

	def three_of_kind(cards):
		return len(cards) >= 3 and cards

	def pair(cards):
		return len(cards) >= 2 and cards[0].value == cards[1].value

	def high_card(cards):
		return cards[0], cards[1:]
